// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { SocketNamespace } from '@bo/common';

const BACKEND_ADDRESS = 'https://boo-project.appspot.com';

export const environment = {
    production: false,
    socketAddress: `${BACKEND_ADDRESS}${SocketNamespace.WEBAPP}`,
    searchRequestsEndpoint: `${BACKEND_ADDRESS}/api/v1/search-requests`,
    searchResultsEndpoint: `${BACKEND_ADDRESS}/api/v1/search-results`,
    authEndpoint: `${BACKEND_ADDRESS}/api/v1/auth`,
    usersEndpoint: `${BACKEND_ADDRESS}/api/v1/users`,
};
