import { SocketNamespace } from '@bo/common';

export const environment = {
    production: true,
    socketAddress: `${SocketNamespace.WEBAPP}`,
    searchRequestsEndpoint: `/api/v1/search-requests`,
    searchResultsEndpoint: `/api/v1/search-results`,
    authEndpoint: `/api/v1/auth`,
    usersEndpoint: `/api/v1/users`,
};
