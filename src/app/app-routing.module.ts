import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { AppRoutes } from '@app/app.routes';

const routes: Routes = [
    {path: '', redirectTo: AppRoutes.USER, pathMatch: 'full'},
    {path: AppRoutes.SEARCH, loadChildren: './search/search.module#SearchModule', canActivate: [AuthGuard]},
    {path: AppRoutes.USER, loadChildren: './user/user.module#UserModule'},
    {path: '**', redirectTo: AppRoutes.USER, pathMatch: 'full'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppRoutingModule {
}
