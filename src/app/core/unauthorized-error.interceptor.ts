import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SnackbarService } from './snackbar.service';
import { AccessService } from 'src/app/core/access.service';

@Injectable({
    providedIn: 'root',
})
export class UnauthorizedErrorInterceptor implements HttpInterceptor {

    constructor(private readonly accessService: AccessService,
                private readonly authService: AuthService,
                private readonly snackbarService: SnackbarService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(catchError(err => {
                const excludedUrl = request.url.includes(this.authService.getExcludedUrlPattern());
                if (!excludedUrl && err.status === 401) {
                    console.error('Intercepted unauthorized error: ', err);
                    this.accessService.logout();
                    this.snackbarService.openError('Log out. Invalid authorization token.');
                    return of() as Observable<HttpEvent<any>>;
                }
                return throwError(err);
            }));
    }
}
