import { Injectable } from '@angular/core';
import { SearchRequestRaw } from '@bo/common';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { FormSearchRequest } from '@core/search-requests/interface/formSearchRequest';
import { SearchRequestFormData } from '@core/search-requests/interface/searchRequestFormData';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { SearchRequestMapper } from '@core/search-requests/utils/SearchRequestMapper';
import { CreateRequestDialogComponent } from '@shared/dialogs/create-request/create-request-dialog.component';
import { ConfirmationDialogComponent } from '@shared/dialogs/confirmation/confirmation-dialog.component';
import { HistoricalDataDialogComponent } from '@shared/dialogs/historical-data/historical-data-dialog.component';
import { ConfirmationDialogData } from '@core/interface/confirmationDialogData';
import { HotelSummary } from '@core/search-results/model/HotelSummary';

@Injectable({
    providedIn: 'root',
})
export class DialogService {

    constructor(private readonly dialog: MatDialog) {
    }

    openCreateSearchReqDialog(data: SearchRequest = null, editingRequest: boolean = false): Observable<SearchRequestRaw> {
        return this.dialog
            .open<CreateRequestDialogComponent, SearchRequestFormData, FormSearchRequest>(CreateRequestDialogComponent, {
                width: '80%',
                height: '70%',
                maxHeight: 600,
                maxWidth: 550,
                minWidth: 350,
                data: {data, editingRequest},
            })
            .afterClosed()
            .pipe(
                filter<FormSearchRequest>(Boolean),
                map((dialogResult) => SearchRequestMapper.fromForm(dialogResult)),
            );
    }

    openConfirmationDialog(data: ConfirmationDialogData): Observable<boolean> {
        return this.dialog
            .open<ConfirmationDialogComponent, ConfirmationDialogData, boolean>(ConfirmationDialogComponent, {
                minWidth: 350,
                maxWidth: 350,
                disableClose: true,
                data,
            })
            .afterClosed()
            .pipe(filter((v) => v !== false));
    }

    openShowResultsHistoryDialog(hotel: HotelSummary): Observable<void> {
        return this.dialog
            .open<HistoricalDataDialogComponent, HotelSummary, void>(HistoricalDataDialogComponent, {
                width: '80%',
                maxHeight: 380,
                maxWidth: 260,
                data: hotel,
            })
            .afterClosed();
    }
}
