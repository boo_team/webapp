import { CheckDate } from '@bo/common';

export class DateFormatter {

    public static NOT_AVAILABLE = 'N/A';

    // 27-01-2019
    public static formatCheckDate = ({day, month, year}: CheckDate): string => {
        const date = new Date(year, month - 1, day);
        return `${date.getDate()}-${DateFormatter.parseToTwoDigitValue(date.getMonth() + 1)}-${date.getFullYear()}`;
    }

    // 'YYYY-MM-DD HH:mm', => 2018-07-31 12:15
    public static formatDateString = (dateString: string): string => {
        if (!dateString) {
            return DateFormatter.NOT_AVAILABLE;
        }
        const parsedDate = new Date(dateString);
        const year = parsedDate.getFullYear();
        const month = DateFormatter.parseToTwoDigitValue((parsedDate.getMonth() + 1));
        const day = DateFormatter.parseToTwoDigitValue(parsedDate.getDate());
        const time = DateFormatter.getTime(parsedDate);
        return `${year}-${month}-${day} ${time.hour}:${time.minutes}`;
    }

    private static getTime(parsedDate: Date): { hour: string, minutes: string } {
        const hour = this.parseToTwoDigitValue(parsedDate.getHours());
        const minutes = this.parseToTwoDigitValue(parsedDate.getMinutes());
        return {hour, minutes};
    }

    private static parseToTwoDigitValue = (value: number): string => `0${value}`.slice(-2);
}
