export interface HotelDto {
    // following parameters always exist
    readonly price: number;
    // following parameters might not be available
    readonly rate: number;
    readonly secondaryRateType: string;
    readonly secondaryRate: number;
    readonly priceWithoutDiscount: number;
    readonly numberOfReviews: number;
    readonly bonuses: string;
    readonly rooms: string;
    // can be changed
    readonly newlyAdded: boolean;
    // parameters different than in hotel.document
    readonly createdAt: string;
}
