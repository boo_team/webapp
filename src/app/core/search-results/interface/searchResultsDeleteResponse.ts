export interface SearchResultsDeleteResponse {
    removedResults: number;
}
