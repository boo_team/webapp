import { HotelSummary } from '@core/search-results/model/HotelSummary';

export interface HotelsFilter {
    active: boolean;
    predicate: HotelsFilterFunc;
    iconActive: string;
    iconInactive: string;
}

export type HotelsFilterFunc = (hotelSummary: HotelSummary) => boolean;
