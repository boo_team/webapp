export interface CoordsDto {
    lat: number;
    lon: number;
}
