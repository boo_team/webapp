export interface BonusesDto {
    readonly freeCancellation: boolean;
    readonly cancelLater: boolean;
    readonly noPrepayment: boolean;
    readonly breakfastIncluded: boolean;
}
