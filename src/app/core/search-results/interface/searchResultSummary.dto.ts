import { HotelSummaryDto } from './hotelSummary.dto';

export interface SearchResultSummaryDto {
    readonly searchId: string;
    readonly firstCreatedAt: string;
    readonly lastCreatedAt: string;
    readonly avgSearchProcessTimeSeconds: number;
    readonly lastSearchProcessTimeSeconds: number;
    readonly avgScrapingTimeSeconds: number;
    readonly lastScrapingTimeSeconds: number;
    readonly resultsCount: number;
    readonly hotelsSummary: HotelSummaryDto[];
}
