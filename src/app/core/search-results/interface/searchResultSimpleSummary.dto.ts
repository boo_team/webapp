export interface SearchResultSimpleSummary {
    readonly _id: string;
    readonly createdAt: string;
    readonly scrapingTimeSeconds: number;
    readonly searchProcessTimeSeconds: number;
    readonly hotelsCount: number;
}
