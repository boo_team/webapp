import { CoordsDto } from './coords.dto';
import { ValueWithDate } from './valueWithDate';
import { BonusesDto } from '@core/search-results/interface/bonuses.dto';

export class HotelSummaryDto {
    // const values
    readonly hotelId: string;
    readonly name: string;
    readonly distanceFromCenterMeters: number;
    readonly districtName: string;
    readonly coords: CoordsDto;
    readonly hotelLink: string;
    readonly propertyType: string;
    readonly starRating: number;
    readonly newlyAdded: boolean;
    // values from last result
    readonly lastPrice: number;
    readonly lastRate: number;
    readonly lastSecondaryRateType: string;
    readonly lastSecondaryRate: number;
    readonly lastPriceBeforeDiscount: number;
    readonly lastNumberOfReviews: number;
    readonly lastBonuses: BonusesDto | null;
    readonly lastBonusesOld: string;
    readonly lastRooms: string;
    // calculated values
    readonly avgPrice: number;
    readonly minPrice: number;
    readonly maxPrice: number;
    readonly avgPriceDiff: number;
    readonly maxPriceDiff: number;
    readonly prices: ValueWithDate<number>[];
}
