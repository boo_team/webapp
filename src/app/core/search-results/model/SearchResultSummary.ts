import { SearchResultSummaryDto } from '@core/search-results/interface/searchResultSummary.dto';
import { DateFormatter } from '@core/utils/DateFormatter';
import { HotelSummaryDto } from '@core/search-results/interface/hotelSummary.dto';
import { HotelSummary } from './HotelSummary';

export class SearchResultSummary {

    public readonly searchId: string;
    public readonly firstCreatedAt: string;
    public readonly lastCreatedAt: string;
    public readonly avgSearchProcessTimeSeconds: number;
    public readonly lastSearchProcessTimeSeconds: number;
    public readonly avgScrapingTimeSeconds: number;
    public readonly lastScrapingTimeSeconds: number;
    public readonly resultsCount: number;
    public readonly hotelsSummary: HotelSummary[];

    constructor({
                    searchId,
                    firstCreatedAt,
                    lastCreatedAt,
                    avgSearchProcessTimeSeconds,
                    lastSearchProcessTimeSeconds,
                    avgScrapingTimeSeconds,
                    lastScrapingTimeSeconds,
                    resultsCount,
                    hotelsSummary,
                }: SearchResultSummaryDto,
                favorites: string[]) {
        this.searchId = searchId;
        this.firstCreatedAt = DateFormatter.formatDateString(firstCreatedAt);
        this.lastCreatedAt = DateFormatter.formatDateString(lastCreatedAt);
        this.avgSearchProcessTimeSeconds = avgSearchProcessTimeSeconds;
        this.lastSearchProcessTimeSeconds = lastSearchProcessTimeSeconds;
        this.avgScrapingTimeSeconds = avgScrapingTimeSeconds;
        this.lastScrapingTimeSeconds = lastScrapingTimeSeconds;
        this.resultsCount = resultsCount;
        this.hotelsSummary = hotelsSummary.map((hotelSummaryDto: HotelSummaryDto) => {
            const isFavorite = !!favorites.find((hotelId) => hotelId === hotelSummaryDto.hotelId);
            return new HotelSummary(hotelSummaryDto, searchId, isFavorite);
        });
    }
}
