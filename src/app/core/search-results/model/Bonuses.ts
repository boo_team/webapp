import { BonusesDto } from '@core/search-results/interface/bonuses.dto';

export class Bonuses {

    public readonly freeCancellation: boolean;
    public readonly cancelLater: boolean;
    public readonly noPrepayment: boolean;
    public readonly breakfastIncluded: boolean;

    constructor({
                    freeCancellation,
                    cancelLater,
                    noPrepayment,
                    breakfastIncluded,
                }: BonusesDto) {
        this.freeCancellation = freeCancellation;
        this.cancelLater = cancelLater;
        this.noPrepayment = noPrepayment;
        this.breakfastIncluded = breakfastIncluded;
    }
}
