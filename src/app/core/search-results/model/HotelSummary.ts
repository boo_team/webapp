import { CoordsDto } from '@core/search-results/interface/coords.dto';
import { ValueWithDate } from '@core/search-results/interface/valueWithDate';
import { HotelSummaryDto } from '@core/search-results/interface/hotelSummary.dto';

export class HotelSummary {

    public readonly parentSearchId: string;
    public readonly hotelId: string;
    public readonly name: string;
    public readonly distanceFromCenterMeters: number;
    public readonly districtName: string;
    public readonly coords: CoordsDto;
    public readonly hotelLink: string;
    public readonly propertyType: string;
    public readonly starRating: number;
    public readonly newlyAdded: boolean;
    public readonly lastPrice: number;
    public readonly lastRate: number;
    public readonly lastSecondaryRateType: string;
    public readonly lastSecondaryRate: number;
    public readonly lastPriceBeforeDiscount: number;
    public readonly lastNumberOfReviews: number;
    public readonly lastRooms: string;
    public readonly lastBonuses: string;
    public readonly avgPrice: number;
    public readonly minPrice: number;
    public readonly maxPrice: number;
    public readonly avgPriceDiff: number;
    public readonly maxPriceDiff: number;
    public readonly prices: ValueWithDate<number>[];
    public readonly priceRate: number;
    public readonly numberOfDetails: string | null;
    public isFavorite: boolean;

    constructor({
                    hotelId,
                    name,
                    distanceFromCenterMeters,
                    districtName,
                    coords,
                    hotelLink,
                    propertyType,
                    starRating,
                    newlyAdded,
                    lastPrice,
                    lastRate,
                    lastSecondaryRateType,
                    lastSecondaryRate,
                    lastPriceBeforeDiscount,
                    lastNumberOfReviews,
                    lastBonusesOld,
                    lastRooms,
                    avgPrice,
                    minPrice,
                    maxPrice,
                    avgPriceDiff,
                    maxPriceDiff,
                    prices,
                }: HotelSummaryDto,
                parentSearchId: string,
                isFavorite: boolean) {
        this.parentSearchId = parentSearchId;
        this.hotelId = hotelId;
        this.name = name;
        this.distanceFromCenterMeters = distanceFromCenterMeters;
        this.districtName = districtName;
        this.coords = coords;
        this.hotelLink = hotelLink;
        this.propertyType = propertyType;
        this.starRating = starRating;
        this.newlyAdded = newlyAdded;
        this.lastPrice = lastPrice;
        this.lastRate = lastRate;
        this.lastSecondaryRateType = lastSecondaryRateType;
        this.lastSecondaryRate = lastSecondaryRate;
        this.lastPriceBeforeDiscount = lastPriceBeforeDiscount;
        this.lastNumberOfReviews = lastNumberOfReviews;
        this.lastRooms = lastRooms;
        this.avgPrice = avgPrice;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.avgPriceDiff = avgPriceDiff;
        this.maxPriceDiff = maxPriceDiff;
        this.prices = prices;
        this.lastBonuses = lastBonusesOld;
        this.priceRate = this.calcPriceRate(lastPrice, maxPrice);
        this.numberOfDetails = this.countDetails({propertyType, lastSecondaryRateType, lastBonuses: lastBonusesOld, lastRooms});
        this.isFavorite = isFavorite;
    }

    private calcPriceRate(lastPrice: number, maxPrice: number): number {
        if (lastPrice === maxPrice) {
            return 0;
        }
        const percent = ((maxPrice - lastPrice) / maxPrice) * 100;
        return Math.round(percent * 100) / 100;
    }

    private countDetails({propertyType, lastSecondaryRateType, lastBonuses, lastRooms}): string | null {
        const numberOfBooleans = [!!propertyType, !!lastSecondaryRateType, !!lastBonuses, !!lastRooms].filter(v => v).length;
        return numberOfBooleans
            ? numberOfBooleans.toString()
            : null;
    }

    setIsFavorite(val: boolean) {
        this.isFavorite = val;
    }
}
