import { Injectable } from '@angular/core';
import { combineLatest, Observable, of, ReplaySubject } from 'rxjs';
import { SearchResultSummary } from '@core/search-results/model/SearchResultSummary';
import { SearchResultsClient } from '@core/search-results/search-results.client';
import { filter, first, map, switchMap, takeWhile } from 'rxjs/operators';
import { UserStore } from '@core/users/user.store';
import { SearchRequestsStore } from '@core/search-requests/search-requests.store';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { HttpErrorResponse } from '@angular/common/http';
import { SearchResultSummaryDto } from '@core/search-results/interface/searchResultSummary.dto';

@Injectable({
    providedIn: 'root',
})
export class SearchResultsStore {

    private readonly searchResults$: ReplaySubject<Map<string, SearchResultSummary>>;

    constructor(private readonly searchRequestsStore: SearchRequestsStore,
                private readonly searchResultsClient: SearchResultsClient,
                private readonly userStore: UserStore) {
        this.searchResults$ = new ReplaySubject(1/**/);
        this.searchResults$.next(new Map<string, SearchResultSummary>());
    }

    public fetchSearchResultSummary(searchId: string,
                                    onError: (error: HttpErrorResponse) => Observable<null>): void {
        combineLatest([
            this.searchResults$,
            this.searchRequestsStore.getSearchRequest(searchId),
        ]).pipe(
            first(),
            takeWhile(this.hasNotBeenAlreadyCalledOrIsObsolete(searchId)),
            switchMap(([resultsMap]: [Map<string, SearchResultSummary>, SearchRequest]) => combineLatest([
                of(resultsMap),
                this.searchResultsClient.getSearchResultSummary(searchId, onError),
                this.userStore.getFavorites(searchId),
            ])),
            map(([resultsMap, summaryDto, favorites]: [Map<string, SearchResultSummary>, SearchResultSummaryDto, string[]]) => {
                const summary = summaryDto ? new SearchResultSummary(summaryDto, favorites) : null;
                return resultsMap.set(searchId, summary);
            }),
        ).subscribe((v) => {
            this.searchResults$.next(v);
        });
    }

    private hasNotBeenAlreadyCalledOrIsObsolete(searchId: string) {
        return ([resultsMap, request]: [Map<string, SearchResultSummary>, SearchRequest]): boolean => {
            if (!resultsMap.has(searchId)) {
                return true;
            }
            if (resultsMap.get(searchId) === null) {
                return false;
            }
            return request.occupancyUpdatedAt > resultsMap.get(searchId).lastCreatedAt;
        };
    }

    public getSearchResultSummary(searchId: string): Observable<SearchResultSummary | null> {
        return this.searchResults$.pipe(
            filter((v) => v.has(searchId)),
            map((v) => v.get(searchId)),
        );
    }
}
