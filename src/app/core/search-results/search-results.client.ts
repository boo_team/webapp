import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpHelperService } from '@core/http-helper.service';
import { SearchResultSummaryDto } from '@core/search-results/interface/searchResultSummary.dto';
import { HotelDto } from '@core/search-results/interface/hotel.dto';

@Injectable({
    providedIn: 'root',
})
export class SearchResultsClient {

    private readonly BASE_URL: string = `${environment.searchResultsEndpoint}`;

    constructor(private readonly httpClient: HttpClient,
                private readonly httpHelperService: HttpHelperService) {
    }

    public getSearchResultsSummary(onErrorResponse: (error: HttpErrorResponse) => Observable<object>): Observable<object> {
        return this.httpClient.get<object>(`${this.BASE_URL}`).pipe(
            catchError<object, Observable<object>>(this.httpHelperService.handleError(`getSearchResultsSummary`, onErrorResponse)),
        );
    }

    public getSearchResultSummary(searchId: string,
                                  onError: (error: HttpErrorResponse) => Observable<null>): Observable<SearchResultSummaryDto | null> {
        return this.httpClient.get<SearchResultSummaryDto>(`${this.BASE_URL}/${encodeURIComponent(searchId)}`).pipe(
            catchError<SearchResultSummaryDto, Observable<null>>(
                this.httpHelperService.handleError(`getSearchResultSummary /${searchId}`, onError)),
        );
    }

    public getHotelsWithHotelId(searchId: string, hotelId: string): Observable<HotelDto[] | void> {
        return this.httpClient.get<HotelDto[]>(`${this.BASE_URL}/${searchId}/${hotelId}`).pipe(
            catchError<HotelDto[], Observable<void>>(
                this.httpHelperService.handleError(`getHotelsWithHotelId /${searchId}/${hotelId}`)),
        );
    }
}
