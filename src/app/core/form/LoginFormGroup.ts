import { FormGroup } from '@angular/forms';
import { CredentialsRaw } from '../interface/credentials.raw';

export class LoginFormGroup extends FormGroup {

    getRawValue(): CredentialsRaw {
        return super.getRawValue();
    }

    get email() {
        return this.get('email');
    }

    get password() {
        return this.get('password');
    }
}
