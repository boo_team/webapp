import { FormGroup } from '@angular/forms';
import { CredentialsRaw } from '@core/interface/credentials.raw';

export class RegisterFormGroup extends FormGroup {

    getRawValue(): CredentialsRaw {
        return super.getRawValue();
    }

    get email() {
        return this.get('email');
    }

    get password() {
        return this.get('password');
    }

    get repeatPassword() {
        return this.get('repeatPassword');
    }
}
