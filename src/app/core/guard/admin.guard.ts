import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { SnackbarService } from '../snackbar.service';
import { map } from 'rxjs/operators';
import { AppRoutes } from '@app/app.routes';
import { AccessService } from '../access.service';

@Injectable({
    providedIn: 'root',
})
export class AdminGuard implements CanActivate {

    constructor(private readonly accessService: AccessService,
                private readonly snackbarService: SnackbarService,
                private readonly router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.accessService.isAdmin().pipe(
            map((adminRights) => {
                if (!adminRights) {
                    this.snackbarService.openError(`You don't have admin rights.`);
                    this.router.navigate([AppRoutes.USER]);
                }
                return adminRights;
            }));
    }
}
