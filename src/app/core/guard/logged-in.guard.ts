import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { SnackbarService } from '../snackbar.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppRoutes } from '@app/app.routes';
import { AccessService } from '../access.service';

@Injectable({
    providedIn: 'root',
})
export class LoggedInGuard implements CanActivate {

    constructor(private readonly accessService: AccessService,
                private readonly snackbarService: SnackbarService,
                private readonly router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.accessService.validateTokenIfPresent().pipe(
            map((validToken: boolean) => {
                if (validToken) {
                    this.snackbarService.open('You are logged in.');
                    this.router.navigate([AppRoutes.SEARCH]);
                }
                return !validToken;
            }),
        );
    }
}
