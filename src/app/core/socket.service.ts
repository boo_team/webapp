import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { CoreModule } from '@app/core/core.module';
import Socket = SocketIOClient.Socket;

@Injectable({
    providedIn: CoreModule,
})
export class SocketService {

    private readonly socket$: Subject<Socket>;

    constructor() {
        this.socket$ = new Subject<Socket>();
    }

    public connect(token: string): Socket {
        const socket = io(`${environment.socketAddress}`, {query: {token}});
        this.socket$.next(socket);
        return socket;
    }

    public disconnect(socket: Socket): void {
        socket.disconnect();
    }

    public getSocket = (): Observable<Socket> => this.socket$;
}
