import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHelperService } from './http-helper.service';
import { Router } from '@angular/router';
import { SnackbarService } from './snackbar.service';
import { TokenPayload } from './interface/tokenPayload';
import { CredentialsRaw } from './interface/credentials.raw';
import { AppRoutes } from '../app.routes';
import { AuthService } from './auth/auth.service';
import { UserDto } from '@core/users/interface/user.dto';
import { map } from 'rxjs/operators';
import { Role } from './users/model/Role';
import { UserStore } from '@core/users/user.store';

@Injectable({
    providedIn: 'root',
})
export class AccessService {

    private readonly ACCESS_TOKEN_STORAGE_KEY = 'access_token';

    constructor(private readonly authService: AuthService,
                private readonly userStore: UserStore,
                private readonly httpClient: HttpClient,
                private readonly httpHelperService: HttpHelperService,
                private readonly router: Router,
                private readonly snackbarService: SnackbarService) {
    }

    public login(credentialsRaw: CredentialsRaw,
                 onErrorResponse: (error: HttpErrorResponse) => Observable<void>): void {
        this.authService.createToken(credentialsRaw, onErrorResponse)
            .subscribe(({accessToken, user}: TokenPayload) => {
                localStorage.setItem(this.ACCESS_TOKEN_STORAGE_KEY, JSON.stringify(accessToken));
                this.userStore.emit(user);
                this.snackbarService.open(`Successfully logged in as: [${credentialsRaw.email}].`);
                this.router.navigate([AppRoutes.SEARCH]);
            });
    }

    public isAdmin = (): Observable<boolean> => this.userStore.getUser()
        .pipe(map((userDto: UserDto) => userDto.roles.some((r) => r.includes(Role.ADMIN))))

    public validateTokenIfPresent(): Observable<boolean | void> {
        if (this.getToken()) {
            return this.authService.verifyToken(this.onVerifyError)
                .pipe(
                    map((userDto: UserDto) => {
                        this.userStore.emit(userDto);
                        return true;
                    }),
                );
        } else {
            return of(false);
        }
    }

    public logout(): void {
        localStorage.removeItem(this.ACCESS_TOKEN_STORAGE_KEY);
        this.router.navigate([AppRoutes.USER]);
    }

    public getToken = (): string | null =>
        (JSON.parse(localStorage.getItem(this.ACCESS_TOKEN_STORAGE_KEY)) as string) || null

    private onVerifyError = (error: HttpErrorResponse): Observable<void> => {
        if (error.status === 401) { // 'UNAUTHORIZED'
            this.snackbarService.openError('Invalid authorization token.', 10000);
        }
        this.logout();
        return of();
    }
}
