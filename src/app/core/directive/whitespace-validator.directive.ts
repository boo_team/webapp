import { AbstractControl, ValidatorFn } from '@angular/forms';

export function whitespaceValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const hasWhitespace = (control.value as string).includes(' ');
        return hasWhitespace
            ? {'containsWhitespace': {value: control.value}}
            : null;
    };
}
