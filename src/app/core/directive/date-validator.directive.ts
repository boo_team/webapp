import { AbstractControl, ValidatorFn } from '@angular/forms';

export function dateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const isDate = control.value instanceof Date;
        return isDate
            ? null
            : {'notInstanceOfDate': {value: control.value}};
    };
}
