import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SnackbarService } from '@core/snackbar.service';

@Injectable({
    providedIn: 'root',
})
export class HttpHelperService {

    constructor(private readonly snackbarService: SnackbarService) {
    }

    public handleError<T>(operation: string, result?: (error?: any) => Observable<T>) {
        return (error): Observable<T> => {
            console.error(operation, error);
            this.snackbarService.openError(`REST ${operation} ${error.message}`);
            return result
                ? result(error)
                : of();
        };
    }
}
