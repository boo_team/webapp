import { Injectable } from '@angular/core';
import { combineLatest, Observable, of, ReplaySubject, Subject } from 'rxjs';
import { UserDto } from '@core/users/interface/user.dto';
import { bufferTime, filter, first, flatMap, map, switchMap } from 'rxjs/operators';
import { UsersClient } from '@core/users/users.client';
import { HttpErrorResponse } from '@angular/common/http';
import { HotelSummary } from '@core/search-results/model/HotelSummary';

@Injectable({
    providedIn: 'root',
})
export class UserStore {

    private readonly user$: ReplaySubject<UserDto>;
    private readonly updateFavorites$: Subject<HotelSummary>;

    constructor(private readonly usersClient: UsersClient) {
        this.user$ = new ReplaySubject(null);
        this.updateFavorites$ = new Subject<HotelSummary>();
        this.subscribeToUpdateFavoritesInBufferTime();
    }

    public emit(userDto: UserDto): void {
        this.user$.next(userDto);
    }

    public getUser = (): Observable<UserDto> => this.user$.asObservable();

    public getFavorites = (searchId: string): Observable<string[]> =>
        this.user$.pipe(
            flatMap(v => v.ownedSearchRequests),
            filter(v => v.searchId === searchId),
            map(v => v.favorites),
        )

    public emitFavoritesChange(hotel: HotelSummary): void {
        this.updateFavorites$.next(hotel);
    }

    public syncUser = (): Observable<void> =>
        this.user$.pipe(
            first(),
            switchMap((user: UserDto) => this.usersClient.getUser(user.email, this.onGetUserError)),
            map((user: UserDto) => {
                this.user$.next(user);
                console.log('User data successfully updated: ', user);
            }),
        )

    private subscribeToUpdateFavoritesInBufferTime(): void {
        this.updateFavorites$.pipe(
            bufferTime(5000),
            filter(v => v.length !== 0),
            map((hotels: HotelSummary[]) =>
                hotels.reduce((lastBufferedHotels: Map<string, HotelSummary>, hotel: HotelSummary) =>
                    lastBufferedHotels.set(hotel.hotelId, hotel), new Map<string, HotelSummary>()),
            ),
            switchMap((lastBufferedHotels: Map<string, HotelSummary>) =>
                combineLatest([of(Array.from(lastBufferedHotels.values())), this.user$.pipe(first())]),
            ),
            map(this.syncWithCurrentFavoritesBeforeUpdate),
            switchMap(({searchId, favorites, user}: { searchId: string, favorites: string[], user: UserDto }) =>
                this.usersClient.updateFavorites(searchId, favorites, this.revertStateOnErrorResponse(user))),
        ).subscribe((userDto: UserDto) => {
            this.user$.next(userDto);
        });
    }

    private syncWithCurrentFavoritesBeforeUpdate =
        ([hotels, user]: [HotelSummary[], UserDto]): { searchId: string, favorites: string[], user: UserDto } => {
            const searchId = hotels[0].parentSearchId;
            const oldFavorites = user.ownedSearchRequests.find(v => v.searchId === searchId).favorites || [];
            const favorites = hotels.reduce((newFavorites: Set<string>, hotel: HotelSummary) => {
                if (hotel.isFavorite) {
                    return newFavorites.add(hotel.hotelId);
                } else {
                    newFavorites.delete(hotel.hotelId);
                    return newFavorites;
                }
            }, new Set<string>(oldFavorites));
            return {
                searchId,
                favorites: Array.from(favorites),
                user,
            };
        }


    private onGetUserError = (error: HttpErrorResponse): Observable<void> => {
        return of();
    }

    private revertStateOnErrorResponse = (userDto: UserDto) =>
        (error: HttpErrorResponse): Observable<UserDto> => {
            console.log('Error when updating favorites. Reverting state of favorites.');
            return of(userDto);
        }
}
