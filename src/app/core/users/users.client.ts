import { Injectable } from '@angular/core';
import { environment } from '@src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHelperService } from '@core/http-helper.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserDto } from '@core/users/interface/user.dto';
import { CredentialsRaw } from '@core/interface/credentials.raw';

@Injectable({
    providedIn: 'root',
})
export class UsersClient {

    private readonly BASE_URL: string = `${environment.usersEndpoint}`;

    constructor(private readonly httpClient: HttpClient,
                private readonly httpHelperService: HttpHelperService) {
    }

    public getUser(email: string,
                   onErrorResponse: (error: HttpErrorResponse) => Observable<void>): Observable<UserDto | void> {
        return this.httpClient.get<UserDto>(`${this.BASE_URL}/${btoa(email)}`).pipe(
            catchError<UserDto, Observable<void>>(this.httpHelperService.handleError(`get user`, onErrorResponse)),
        );
    }

    public createUser({email, password}: CredentialsRaw,
                      onErrorResponse: (error: HttpErrorResponse) => Observable<void>): Observable<UserDto | void> {
        return this.httpClient.post<UserDto>(`${this.BASE_URL}`, {email, password}).pipe(
            catchError<UserDto, Observable<void>>(this.httpHelperService.handleError(`create user`, onErrorResponse)),
        );
    }

    public updateUser(partialUpdate: Partial<UserDto>,
                      onErrorResponse: (error: HttpErrorResponse) => Observable<void>): Observable<UserDto | void> {
        return this.httpClient.patch<Partial<UserDto>>(`${this.BASE_URL}`, partialUpdate).pipe(
            catchError<UserDto, Observable<void>>(this.httpHelperService.handleError(`update user`, onErrorResponse)),
        );
    }

    public updateFavorites(searchId: string,
                           favorites: string[],
                           onErrorResponse: (error: HttpErrorResponse) => Observable<UserDto>): Observable<UserDto> {
        return this.httpClient
            .post<UserDto>(`${this.BASE_URL}/favorites/${searchId}`, {favorites})
            .pipe(
                catchError<UserDto, Observable<UserDto>>(this.httpHelperService.handleError(`update favorites`, onErrorResponse)),
            );
    }
}
