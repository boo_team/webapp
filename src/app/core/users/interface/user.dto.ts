import { Role } from '@core/users/model/Role';
import { OwnedRequestDto } from '@core/users/interface/ownedRequest.dto';

export interface UserDto {
    email: string;
    ownedSearchRequests: OwnedRequestDto[];
    roles: Role[];
}
