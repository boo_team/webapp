import { Injectable } from '@angular/core';
import { SocketService } from '@core/socket.service';
import { fromEvent, Observable, Subject } from 'rxjs';
import { catchError, filter, switchMap } from 'rxjs/operators';
import { SearchStatusDto, SocketEvent } from '@bo/common';
import Socket = SocketIOClient.Socket;

@Injectable({
    providedIn: 'root',
})
export class SearchStatusService {

    private readonly onSearchStatus$: Subject<SearchStatusDto>;

    constructor(private readonly socketService: SocketService) {
        this.onSearchStatus$ = new Subject();
        this.socketService.getSocket().pipe(
            switchMap<Socket, Observable<SearchStatusDto>>((socket) => fromEvent(socket, SocketEvent.SEARCH_STATUS)),
            catchError((err) => {
                console.error(err);
                return null;
            }),
        ).subscribe(this.onSearchStatus$);
    }

    public getSearchStatus = (searchId: string): Observable<SearchStatusDto> => this.onSearchStatus$.pipe(
        filter(v => v.searchId === searchId)
    )
}
