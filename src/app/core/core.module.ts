import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from '@core/auth/auth.interceptor';
import { UnauthorizedErrorInterceptor } from '@core/unauthorized-error.interceptor';

@NgModule({
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: UnauthorizedErrorInterceptor, multi: true},
    ],
    declarations: [],
    imports: [
        CommonModule,
        HttpClientModule,
    ],
})
export class CoreModule {
}
