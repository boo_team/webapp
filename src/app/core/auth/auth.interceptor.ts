/*
 * Copyright 2018 Jeppesen. All Rights Reserved
 */
import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccessService } from '@core/access.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private readonly accessService: AccessService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.accessService.getToken();
        if (token) {
            const request = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`,
                },
            });
            return next.handle(request);
        } else {
            return next.handle(req);
        }
    }
}
