import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpHelperService } from '@app/core/http-helper.service';
import { CredentialsRaw } from '@app/core/interface/credentials.raw';
import { TokenPayload } from '@core/interface/tokenPayload';
import { UserDto } from '@core/users/interface/user.dto';

@Injectable({
    providedIn: 'root',
})
export class AuthService {

    private readonly BASE_URL: string = `${environment.authEndpoint}`;

    constructor(private readonly httpClient: HttpClient,
                private readonly httpHelperService: HttpHelperService) {
    }

    public createToken({email, password}: CredentialsRaw,
                       onErrorResponse: (error: HttpErrorResponse) => Observable<void>): Observable<TokenPayload | void> {
        return this.httpClient.post<TokenPayload>(`${this.BASE_URL}/token`, {email, password}).pipe(
            catchError<TokenPayload, Observable<void>>(this.httpHelperService.handleError(`token`, onErrorResponse)),
        );
    }

    public verifyToken(onErrorResponse: (error: HttpErrorResponse) => Observable<void>): Observable<UserDto | void> {
        return this.httpClient.get<UserDto>(`${this.BASE_URL}/verify`).pipe(
            catchError(this.httpHelperService.handleError('verify', onErrorResponse)),
        );
    }

    public getExcludedUrlPattern = (): string => this.BASE_URL;
}
