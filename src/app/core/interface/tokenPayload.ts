import { UserDto } from '@core/users/interface/user.dto';

export interface TokenPayload {
    user: UserDto;
    accessToken: string;
}
