import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

    constructor(private readonly snackBar: MatSnackBar) {
    }

    public open(message: string, duration = 5000): MatSnackBarRef<SimpleSnackBar> {
        return this.snackBar.open(message, 'Close', {
            duration,
            panelClass: ['snackbar'],
        });
    }

    public openError(message: string, duration = 5000): MatSnackBarRef<SimpleSnackBar> {
        return this.snackBar.open(message, 'Close', {
            duration,
            panelClass: ['snackbar', 'error-snackbar'],
        });
    }
}
