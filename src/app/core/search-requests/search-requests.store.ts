import { Injectable } from '@angular/core';
import { SocketService } from '@core/socket.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SnackbarService } from '@core/snackbar.service';
import { AccessService } from '@core/access.service';
import { BehaviorSubject, fromEvent, Observable, of } from 'rxjs';
import { SearchRequestDto, SearchRequestRaw, SocketEvent } from '@bo/common';
import { filter, flatMap, map, switchMap, tap } from 'rxjs/operators';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { SearchRequestsClient } from '@core/search-requests/search-requests.client';
import { UserStore } from '@core/users/user.store';
import Socket = SocketIOClient.Socket;

@Injectable({
    providedIn: 'root',
})
export class SearchRequestsStore {

    // TODO: change to Map
    private readonly onSearchRequests$: BehaviorSubject<SearchRequestDto[]>;

    constructor(private readonly accessService: AccessService,
                private readonly searchRequestsClient: SearchRequestsClient,
                private readonly snackbarService: SnackbarService,
                private readonly socketService: SocketService,
                private readonly userStore: UserStore) {
        this.onSearchRequests$ = new BehaviorSubject<SearchRequestDto[]>([]);
        this.socketService.getSocket().pipe(
            tap((socket) => fromEvent<string>(socket, 'unauthorized_error').subscribe(this.onWsUnauthorizedError)),
            switchMap<Socket, Observable<SearchRequestDto[]>>((socket) => fromEvent(socket, SocketEvent.SEARCH_REQUESTS)),
        ).subscribe(this.onSearchRequests$);
        this.onSearchRequests$.subscribe();
    }

    public getSearchRequests = (): Observable<SearchRequest[]> => this.onSearchRequests$.pipe(
        map((s) => s.map(v => new SearchRequest(v))))

    public getSearchRequest = (searchId: string): Observable<SearchRequest> => this.getSearchRequests().pipe(
        flatMap(v => v),
        filter(v => v.searchId === searchId),
    )

    public createSearchRequest = (searchRequestRaw: SearchRequestRaw,
                                  onSuccess: (searchRequestDto: SearchRequestDto) => void): Observable<SearchRequestDto | void> =>
        this.handleRequestAndSyncUserAfterSuccess(
            () => this.searchRequestsClient.createSearchRequest(searchRequestRaw, this.onErrorResponse), onSuccess)

    public updateSearchRequest = (oldSearchId: string,
                                  searchRequestRaw: SearchRequestRaw,
                                  onSuccess: (searchRequestDto: SearchRequestDto) => void): Observable<SearchRequestDto | void> =>
        this.handleRequestAndSyncUserAfterSuccess(
            () => this.searchRequestsClient.updateSearchRequest(oldSearchId, searchRequestRaw, this.onErrorResponse), onSuccess)

    public deleteSearchRequest = (searchId: string,
                                  onSuccess: (res: string) => void): Observable<string | void> =>
        this.handleRequestAndSyncUserAfterSuccess(() => this.searchRequestsClient.deleteSearchRequest(searchId), onSuccess)

    private handleRequestAndSyncUserAfterSuccess<T>(request: () => Observable<T | void>,
                                                    onSuccess: (res: T) => void): Observable<void> {
        return request().pipe(
            tap(onSuccess),
            switchMap(() => this.userStore.syncUser()),
        );
    }

    private onErrorResponse = (error: HttpErrorResponse): Observable<void> => {
        if (error.status === 409) { // 'CONFLICT'
            this.snackbarService.openError('Request with these parameters already exists.', 10000);
        }
        if (error.status === 406) { // 'NOT_ACCEPTABLE'
            this.snackbarService.openError('You have exceeded the limit of requests.', 10000);
        }
        return of();
    }

    private onWsUnauthorizedError = (message: string) => {
        console.error('Socket unauthorized error: ', message);
        this.snackbarService.openError(`Socket unauthorized error: ${message}`, 10000);
        this.accessService.logout();
    }
}

