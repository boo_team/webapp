import { Language, SearchRequestDto } from '@bo/common';
import { SearchRequestStatus } from '@core/search-requests/model/SearchRequestStatus';
import { SearchRequestHelper } from '@core/search-requests/utils/SearchRequestHelper';
import { DateFormatter } from '@core/utils/DateFormatter';
import { FormattedCheckDate } from '@core/search-requests/model/FormattedCheckDate';
import { Currency } from '@bo/common/lib/search-request/enums/Currency';
import { FormattedNumberOfChildren } from '@core/search-requests/model/FormattedNumberOfChildren';

export class SearchRequest {

    public readonly searchId: string;
    public readonly city: string;
    public readonly checkInDate: FormattedCheckDate;
    public readonly checkOutDate: FormattedCheckDate;
    public readonly priority: number;
    public readonly resultsLimit: number;
    public readonly updateFrequencyMinutes: number;
    public readonly numberOfAdults: number;
    public readonly numberOfRooms: number;
    public readonly numberOfChildren: FormattedNumberOfChildren | null;
    public readonly updatedAt: string;
    public readonly createdAt: string;
    public readonly occupancyUpdatedAt: string;
    public readonly currency: Currency;
    public readonly language: Language;

    private _status: SearchRequestStatus;

    constructor(searchRequestDto: SearchRequestDto) {
        this.searchId = searchRequestDto.searchId;
        this.city = searchRequestDto.city;
        this.checkInDate = new FormattedCheckDate(searchRequestDto.checkInDate);
        this.checkOutDate = new FormattedCheckDate(searchRequestDto.checkOutDate);
        this.priority = searchRequestDto.priority;
        this.resultsLimit = searchRequestDto.resultsLimit;
        this.updateFrequencyMinutes = searchRequestDto.updateFrequencyMinutes;
        this.numberOfAdults = searchRequestDto.numberOfAdults;
        this.numberOfRooms = searchRequestDto.numberOfRooms;
        this.numberOfChildren = searchRequestDto.numberOfChildren && searchRequestDto.numberOfChildren.length
            ? new FormattedNumberOfChildren(searchRequestDto.numberOfChildren)
            : null;
        this.updatedAt = DateFormatter.formatDateString(searchRequestDto.updatedAt);
        this.createdAt = DateFormatter.formatDateString(searchRequestDto.createdAt);
        this.occupancyUpdatedAt = DateFormatter.formatDateString(searchRequestDto.occupancyUpdatedAt);
        this.currency = searchRequestDto.currency;
        this.language = searchRequestDto.language;
        this._status = SearchRequestHelper.setStatus(searchRequestDto);
    }

    get status(): SearchRequestStatus {
        return this._status;
    }

    set status(value: SearchRequestStatus) {
        this._status = value;
    }
}
