import { ChildrenProperties } from '@bo/common/lib/search-request/interfaces/childrenProperties';
import { FormChildrenProperties } from '@core/search-requests/interface/formChildrenProperties';

export class FormattedNumberOfChildren {

    public readonly formFormat: FormChildrenProperties[];
    public readonly stringFormat: string;

    constructor(numberOfChildren: ChildrenProperties[]) {
        this.formFormat = numberOfChildren.map((v) => ({yearAgeAtCheckOut: String(v.yearAgeAtCheckOut)}));
        this.stringFormat = numberOfChildren.map((v) => `age: ${v.yearAgeAtCheckOut}`).join(', ');
    }

    public static fromForm = (formFormat: FormChildrenProperties[]): ChildrenProperties[] =>
        formFormat.map((v) => ({yearAgeAtCheckOut: Number(v.yearAgeAtCheckOut)}))
}
