import { CheckDate } from '@bo/common/lib/search-request/interfaces/checkDate';
import { DateFormatter } from '../../utils/DateFormatter';

export class FormattedCheckDate {

    public readonly stringFormat: string;
    public readonly dateFormat: Date;

    constructor(checkDate: CheckDate) {
        this.stringFormat = DateFormatter.formatCheckDate(checkDate);
        this.dateFormat = new Date(checkDate.year, checkDate.month - 1, checkDate.day);
    }

    public static fromDate(date: Date): CheckDate {
        return {
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear(),
        };
    }
}
