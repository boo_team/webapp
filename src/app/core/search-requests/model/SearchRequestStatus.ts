export enum SearchRequestStatus {
    IN_PROGRESS = 'IN PROGRESS',
    UPDATED = 'UPDATED',
    OLD = 'OLD',
    INVALID = 'INVALID',
}
