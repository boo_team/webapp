import { Currency } from '@bo/common/lib/search-request/enums/Currency';
import { Language } from '@bo/common/lib/search-request/enums/Language';
import { FormChildrenProperties } from '@core/search-requests/interface/formChildrenProperties';

export interface FormSearchRequest {
    readonly priority: string;
    readonly updateFrequencyMinutes: string;
    readonly resultsLimit: string;
    // Scenario parameters
    readonly city: string;
    readonly checkInDate: Date;
    readonly checkOutDate: Date;
    readonly numberOfRooms: string;
    readonly numberOfAdults: string;
    readonly numberOfChildren: FormChildrenProperties[];
    readonly currency: Currency;
    readonly language: Language;
}
