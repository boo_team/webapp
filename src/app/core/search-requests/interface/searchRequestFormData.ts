import { SearchRequest } from '@core/search-requests/model/SearchRequest';

export interface SearchRequestFormData {
    readonly data: SearchRequest;
    readonly editingRequest: boolean;
}
