import { FormSearchRequest } from '@core/search-requests/interface/formSearchRequest';
import { SearchRequestRaw } from '@bo/common';
import { FormattedCheckDate } from '@core/search-requests/model/FormattedCheckDate';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { FormattedNumberOfChildren } from '@core/search-requests/model/FormattedNumberOfChildren';

export class SearchRequestMapper {

    public static fromForm(formSearchRequest: FormSearchRequest): SearchRequestRaw {
        const {
            priority, updateFrequencyMinutes, resultsLimit, numberOfRooms,
            numberOfAdults, numberOfChildren, checkInDate, checkOutDate,
        } = formSearchRequest;
        return {
            ...formSearchRequest,
            priority: Number(priority),
            updateFrequencyMinutes: Number(updateFrequencyMinutes),
            resultsLimit: Number(resultsLimit),
            numberOfRooms: Number(numberOfRooms),
            numberOfAdults: Number(numberOfAdults),
            numberOfChildren: FormattedNumberOfChildren.fromForm(numberOfChildren),
            checkInDate: FormattedCheckDate.fromDate(checkInDate),
            checkOutDate: FormattedCheckDate.fromDate(checkOutDate),
        };
    }

    public static toForm(searchRequest: SearchRequest): FormSearchRequest {
        const {
            priority, updateFrequencyMinutes, city, resultsLimit, numberOfRooms,
            numberOfAdults, numberOfChildren, checkInDate, checkOutDate, currency, language,
        } = searchRequest;
        return {
            priority: String(priority),
            updateFrequencyMinutes: String(updateFrequencyMinutes),
            resultsLimit: String(resultsLimit),
            city,
            checkInDate: checkInDate.dateFormat,
            checkOutDate: checkOutDate.dateFormat,
            numberOfRooms: String(numberOfRooms),
            numberOfAdults: String(numberOfAdults),
            numberOfChildren: numberOfChildren ? numberOfChildren.formFormat : [],
            currency,
            language,
        };
    }
}
