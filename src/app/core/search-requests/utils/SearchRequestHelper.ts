import { CheckDate, SearchRequestDto, SearchStatusDto, WebSocketClient } from '@bo/common';
import { SearchRequestStatus } from '@core/search-requests/model/SearchRequestStatus';

export class SearchRequestHelper {

    public static setStatus({
                                checkInDate,
                                occupiedBy,
                                updateFrequencyMinutes,
                                occupancyUpdatedAt,
                            }: SearchRequestDto): SearchRequestStatus {
        if (this.isExpiredDate(checkInDate)) {
            return SearchRequestStatus.INVALID;
        }
        if (occupiedBy !== WebSocketClient.NONE.dynamicId()) {
            return SearchRequestStatus.IN_PROGRESS;
        }
        if (this.isOld(updateFrequencyMinutes, occupancyUpdatedAt)) {
            return SearchRequestStatus.OLD;
        }
        return SearchRequestStatus.UPDATED;
    }

    public static isInProgress = (searchStatus: SearchStatusDto): SearchRequestStatus =>
        searchStatus.scrapingEnd
            ? SearchRequestStatus.UPDATED
            : SearchRequestStatus.IN_PROGRESS

    private static isOld = (updateFrequencyMinutes: number, occupancyUpdatedAt: string) =>
        (Date.now() - (updateFrequencyMinutes * 60000)) > new Date(occupancyUpdatedAt).valueOf()

    private static isExpiredDate = ({year, month, day}: CheckDate): boolean => {
        const now = new Date();
        const previousDay = now.setDate(now.getDate() - 1).valueOf();
        return previousDay > (new Date(year, month - 1, day).valueOf());
    }
}
