import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchRequestDto, SearchRequestRaw } from '@bo/common';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHelperService } from '@core/http-helper.service';
import { environment } from '@src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class SearchRequestsClient {

    private readonly BASE_URL: string = `${environment.searchRequestsEndpoint}`;

    constructor(private readonly httpClient: HttpClient,
                private readonly httpHelperService: HttpHelperService) {
    }

    public createSearchRequest(searchRequestRaw: SearchRequestRaw,
                               onErrorResponse: (error: HttpErrorResponse) => Observable<void>): Observable<SearchRequestDto | void> {
        return this.httpClient.post<SearchRequestDto>(`${this.BASE_URL}`, searchRequestRaw).pipe(
            catchError<SearchRequestDto, Observable<void>>(this.httpHelperService.handleError(`createSearchRequest`, onErrorResponse)),
        );
    }

    public updateSearchRequest(oldSearchId: string,
                               searchRequestRaw: SearchRequestRaw,
                               onErrorResponse: (error: HttpErrorResponse) => Observable<void>): Observable<SearchRequestDto | void> {
        return this.httpClient.put<SearchRequestDto>(`${this.BASE_URL}/${oldSearchId}`, searchRequestRaw).pipe(
            catchError<SearchRequestDto, Observable<void>>(
                this.httpHelperService.handleError(`updateSearchRequest /${oldSearchId}`, onErrorResponse)),
        );
    }

    public deleteSearchRequest(searchId: string): Observable<string | void> {
        return this.httpClient.delete(`${this.BASE_URL}/${searchId}`, {responseType: 'text'}).pipe(
            catchError<string, Observable<string | void>>(this.httpHelperService.handleError(`deleteSearchRequest /${searchId}`)),
        );
    }
}
