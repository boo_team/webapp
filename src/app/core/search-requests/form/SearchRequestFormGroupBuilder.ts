import { SearchRequestFormGroup } from '@core/search-requests/form/SearchRequestFormGroup';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { dateValidator } from '@core/directive/date-validator.directive';
import { Currency, Language } from '@bo/common';

export class SearchRequestFormGroupBuilder {

    public readonly minNumberOfRooms = 1;
    public readonly maxNumberOfRooms = 30;

    public readonly minNumberOfAdults = 1;
    public readonly maxNumberOfAdults = 30;

    public readonly minAgeAtCheckOut = 0;
    public readonly maxAgeAtCheckOut = 17;

    public readonly minPriority = 0;
    public readonly maxPriority = 100;

    public readonly minSearchResultsLimit = 100;
    public readonly maxSearchResultsLimit = 600;

    public readonly minUpdateFrequencyMinutes = 5;
    public readonly maxUpdateFrequencyMinutes = 1440;

    public currencyEnums: string[] = Object.keys(Currency);
    public selectedCurrency = Currency.PLN;

    public languageEnums: string[] = Object.keys(Language);
    public selectedLanguage = Language.ENGLISH;

    get = (): SearchRequestFormGroup =>
        new SearchRequestFormGroup({
            city: new FormControl('',
                [Validators.required, Validators.pattern(/^[a-zA-Z\sżźćńółęąśŻŹĆĄŚĘŁÓŃ]*$/)]),
            checkInDate: new FormControl('', [Validators.required, dateValidator()]),
            checkOutDate: new FormControl({value: '', disabled: true}, [Validators.required, dateValidator()]),
            numberOfRooms: new FormControl(1,
                [Validators.required, Validators.min(this.minNumberOfRooms), Validators.max(this.maxNumberOfRooms)]),
            numberOfAdults: new FormControl(2,
                [Validators.required, Validators.min(this.minNumberOfAdults), Validators.max(this.maxNumberOfAdults)]),
            numberOfChildren: new FormArray([]),
            priority: new FormControl(0,
                [Validators.required, Validators.min(this.minPriority), Validators.max(this.maxPriority)]),
            resultsLimit: new FormControl(200,
                [Validators.required, Validators.min(this.minSearchResultsLimit), Validators.max(this.maxSearchResultsLimit)]),
            updateFrequencyMinutes: new FormControl(120,
                [Validators.required, Validators.min(this.minUpdateFrequencyMinutes), Validators.max(this.maxUpdateFrequencyMinutes)]),
            currency: new FormControl({value: Currency.PLN, disabled: true}, [Validators.required]),
            language: new FormControl({value: Language.ENGLISH, disabled: true}, [Validators.required]),
        })

    getChildGroup = (ageAtCheckOut: string = '0'): FormGroup => new FormGroup({
        yearAgeAtCheckOut: new FormControl(ageAtCheckOut,
            [Validators.required, Validators.min(this.minAgeAtCheckOut), Validators.max(this.maxAgeAtCheckOut)]),
    })
}
