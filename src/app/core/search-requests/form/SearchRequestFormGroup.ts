import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { SearchRequestRaw } from '@bo/common';
import { FormSearchRequest } from '@core/search-requests/interface/formSearchRequest';

export class SearchRequestFormGroup extends FormGroup {
    setValue(value: FormSearchRequest, options?: { onlySelf?: boolean; emitEvent?: boolean }): void {
        super.setValue(value, options);
    }

    getRawValue(): FormSearchRequest {
        return super.getRawValue();
    }

    get(key: keyof SearchRequestRaw): AbstractControl {
        return super.get(key);
    }

    get city() {
        return this.get('city');
    }

    get checkInDate() {
        return this.get('checkInDate');
    }

    get checkOutDate() {
        return this.get('checkOutDate');
    }

    get numberOfRooms() {
        return this.get('numberOfRooms');
    }

    get numberOfAdults() {
        return this.get('numberOfAdults');
    }

    get numberOfChildren() {
        return (this.get('numberOfChildren') as FormArray).controls;
    }

    get numberOfChildrenFormArray() {
        return this.get('numberOfChildren') as FormArray;
    }

    get priority() {
        return this.get('priority');
    }

    get resultsLimit() {
        return this.get('resultsLimit');
    }

    get updateFrequencyMinutes() {
        return this.get('updateFrequencyMinutes');
    }
}
