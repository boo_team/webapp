import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { LoginFormGroup } from '@core/form/LoginFormGroup';
import { Router } from '@angular/router';
import { SnackbarService } from '@core/snackbar.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AccessService } from '@core/access.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent {

    public loginFormGroup: LoginFormGroup;
    public loggingIn = false;

    constructor(private readonly accessService: AccessService,
                private readonly router: Router,
                private readonly snackbarService: SnackbarService) {
        this.loginFormGroup = new LoginFormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', [Validators.required]),
        });
    }

    public onFormSubmit(): void {
        const credentialsRaw = this.loginFormGroup.getRawValue();
        this.loggingIn = true;
        this.accessService.login(credentialsRaw, this.onUnauthorizedResponse);
    }

    private onUnauthorizedResponse = (error: HttpErrorResponse): Observable<void> => {
        this.loggingIn = false;
        if (error.status === 401) { // 'UNAUTHORIZED'
            this.snackbarService.openError('Invalid email or password.', 10000);
        }
        return of();
    }
}
