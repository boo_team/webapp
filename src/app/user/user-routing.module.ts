import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '@app/user/login/login.component';
import { UserComponent } from '@user/user.component';
import { RegisterComponent } from '@user/register/register.component';
import { LoggedInGuard } from '@core/guard/logged-in.guard';

const routes: Routes = [
    {
        path: '', component: UserComponent,
        children: [
            {path: '', redirectTo: 'login', pathMatch: 'full'},
            {path: 'login', component: LoginComponent, canActivate: [LoggedInGuard]},
            {path: 'register', component: RegisterComponent},
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserRoutingModule {
}
