import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LoginComponent } from '@app/user/login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        SharedModule,
    ],
    declarations: [
        LoginComponent,
        RegisterComponent,
        UserComponent,
    ],
})
export class UserModule {
}
