import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SnackbarService } from '@core/snackbar.service';
import { FormControl, Validators } from '@angular/forms';
import { whitespaceValidator } from '@core/directive/whitespace-validator.directive';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { RegisterFormGroup } from '@core/form/RegisterFormGroup';
import { UsersClient } from '@core/users/users.client';
import { AppRoutes } from '@app/app.routes';
import { UserDto } from '@core/users/interface/user.dto';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {

    public registerFormGroup: RegisterFormGroup;
    public registering = false;

    constructor(private readonly usersClient: UsersClient,
                private readonly router: Router,
                private readonly snackbarService: SnackbarService) {
        this.registerFormGroup = new RegisterFormGroup({
            email: new FormControl('', [Validators.required, whitespaceValidator(), Validators.email]),
            password: new FormControl('', [Validators.required, Validators.minLength(3)]),
            repeatPassword: new FormControl('', [Validators.required]),
        });

        this.registerFormGroup.repeatPassword.valueChanges.subscribe(value => {
            const pass = this.registerFormGroup.password.value;
            if (value === pass) {
                this.registerFormGroup.repeatPassword.setErrors(null);
            } else {
                this.registerFormGroup.repeatPassword.setErrors({unmatchedPassword: true});
            }
        });

        this.registerFormGroup.password.valueChanges.subscribe(value => {
            this.registerFormGroup.repeatPassword.setValue(null);
        });
    }

    public onFormSubmit(): void {
        const userRaw = this.registerFormGroup.getRawValue();
        this.registering = true;
        this.usersClient.createUser(userRaw, this.onConflictResponse)
            .subscribe(({email}: UserDto) => {
                this.snackbarService.open(`User: [${email}] successfully registered`);
                this.router.navigate([AppRoutes.USER]);
            });
    }

    private onConflictResponse = (error: HttpErrorResponse): Observable<void> => {
        this.registering = false;
        if (error.status === 409) { // 'CONFLICT'
            this.snackbarService.openError(`Email: '${this.registerFormGroup.email.value}' already exist.`, 10000);
            this.registerFormGroup.password.setValue('');
        }
        return of();
    }
}
