import { NgModule } from '@angular/core';
import {
    ErrorStateMatcher, MatBadgeModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    ShowOnDirtyErrorStateMatcher,
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { HistoricalDataDialogComponent } from '@shared/dialogs/historical-data/historical-data-dialog.component';
import { CreateRequestDialogComponent } from '@shared/dialogs/create-request/create-request-dialog.component';
import { ConfirmationDialogComponent } from '@shared/dialogs/confirmation/confirmation-dialog.component';
import { CommonModule } from '@angular/common';

const MATERIAL_MODULES = [
    MatButtonModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatCardModule,
    MatExpansionModule,
    MatProgressBarModule,
    FlexLayoutModule,
    MatTableModule,
    MatSelectModule,
    MatPaginatorModule,
    MatSortModule,
    MatDatepickerModule,
    MatDialogModule,
    MatNativeDateModule,
    MatIconModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatTooltipModule,
    MatBadgeModule,
];

@NgModule({
    imports: [
        CommonModule,
        NgxJsonViewerModule,
        ...MATERIAL_MODULES,
    ],
    exports: [
        NgxJsonViewerModule,
        ...MATERIAL_MODULES,
    ],
    providers: [
        {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    ],
    declarations: [
        HistoricalDataDialogComponent,
        CreateRequestDialogComponent,
        ConfirmationDialogComponent,
    ],
    entryComponents: [
        HistoricalDataDialogComponent,
        CreateRequestDialogComponent,
        ConfirmationDialogComponent,
    ],
})
export class SharedModule {
}
