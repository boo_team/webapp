import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { SearchRequestFormGroup } from '@core/search-requests/form/SearchRequestFormGroup';
import { SearchRequestFormGroupBuilder } from '@core/search-requests/form/SearchRequestFormGroupBuilder';
import { SearchRequestMapper } from '@core/search-requests/utils/SearchRequestMapper';
import { FormSearchRequest } from '@core/search-requests/interface/formSearchRequest';
import { SearchRequestFormData } from '@core/search-requests/interface/searchRequestFormData';
import { FormArray } from '@angular/forms';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { FormChildrenProperties } from '@core/search-requests/interface/formChildrenProperties';
import { AccessService } from '@core/access.service';

@Component({
    selector: 'app-create-request-dialog',
    templateUrl: './create-request-dialog.component.html',
    styleUrls: ['./create-request-dialog.component.scss'],
})
export class CreateRequestDialogComponent implements OnInit, OnDestroy {

    public builder: SearchRequestFormGroupBuilder;
    public searchRequestFormGroup: SearchRequestFormGroup;
    public editingRequest = false;

    public readonly minCheckInDate = new Date();
    public minCheckOutDate = null;
    public maxCheckOutDate = null;

    private readonly DAY_MS = 1000 * 60 * 60 * 24;
    private readonly MONTH_MS = this.DAY_MS * 30;
    private statusChangeSub: Subscription;

    constructor(public dialogRef: MatDialogRef<CreateRequestDialogComponent, FormSearchRequest>,
                private readonly accessService: AccessService,
                @Inject(MAT_DIALOG_DATA) public searchRequestFormData: SearchRequestFormData) {
        const searchRequestFormGroupBuilder = new SearchRequestFormGroupBuilder();
        this.builder = searchRequestFormGroupBuilder;
        this.searchRequestFormGroup = searchRequestFormGroupBuilder.get();
        this.statusChangeSub = this.searchRequestFormGroup.checkInDate.statusChanges
            .subscribe((status: string) => {
                if (status === 'VALID') {
                    this.searchRequestFormGroup.checkOutDate.enable();
                    const checkInDate: Date = this.searchRequestFormGroup.checkInDate.value;
                    const dayAfterCheckIn = new Date(checkInDate.valueOf() + this.DAY_MS);
                    this.searchRequestFormGroup.checkOutDate.setValue(dayAfterCheckIn);
                    this.minCheckOutDate = dayAfterCheckIn;
                    this.maxCheckOutDate = new Date(checkInDate.valueOf() + this.MONTH_MS);
                }
            });
    }

    ngOnInit() {
        const {data: searchRequest, editingRequest} = this.searchRequestFormData;
        if (searchRequest) {
            this.overrideSearchRequestFormGroup(this.searchRequestFormGroup, searchRequest);
        }
        this.editingRequest = editingRequest;
    }

    addChild(): void {
        const lastElementIdx = this.searchRequestFormGroup.numberOfChildren.length - 1;
        const lastElement = this.searchRequestFormGroup.numberOfChildren[lastElementIdx];
        let childGroup = this.builder.getChildGroup();
        if (lastElement) {
            const lastValue = (lastElement.value as FormChildrenProperties).yearAgeAtCheckOut;
            childGroup = this.builder.getChildGroup(lastValue);
        }
        this.searchRequestFormGroup.numberOfChildrenFormArray.push(childGroup);
    }

    isAdmin = (): Observable<boolean> => this.accessService.isAdmin();

    removeChild(idx: number): void {
        this.searchRequestFormGroup.numberOfChildrenFormArray.removeAt(idx);
    }

    ngOnDestroy(): void {
        this.statusChangeSub.unsubscribe();
    }

    onFormSubmit(): void {
        this.dialogRef.close(this.searchRequestFormGroup.getRawValue());
    }

    private overrideSearchRequestFormGroup(searchRequestFormGroup: SearchRequestFormGroup,
                                           searchRequest: SearchRequest): void {
        const formSearchRequest = SearchRequestMapper.toForm(searchRequest);
        searchRequestFormGroup.patchValue(formSearchRequest);
        // due to FormArray it needs to be done on this way
        searchRequestFormGroup.setControl('numberOfChildren', new FormArray(formSearchRequest.numberOfChildren.map(
            (v) => new SearchRequestFormGroupBuilder().getChildGroup(String(v.yearAgeAtCheckOut)))));
    }
}


