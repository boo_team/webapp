import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalDataDialogComponent } from './historical-data-dialog.component';

describe('HistoricalDataDialogComponent', () => {
  let component: HistoricalDataDialogComponent;
  let fixture: ComponentFixture<HistoricalDataDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalDataDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalDataDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
