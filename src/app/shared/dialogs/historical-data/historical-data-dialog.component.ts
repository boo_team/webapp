import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { DateFormatter } from '@core/utils/DateFormatter';
import { HotelSummary } from '@core/search-results/model/HotelSummary';

@Component({
    selector: 'app-historical-data-dialog',
    templateUrl: './historical-data-dialog.component.html',
    styleUrls: ['./historical-data-dialog.component.scss'],
})
export class HistoricalDataDialogComponent implements OnInit {

    public prices: Array<{ date: string, price: number }>;

    constructor(@Inject(MAT_DIALOG_DATA) public data: HotelSummary) {
    }

    ngOnInit() {
        this.prices = this.data.prices.map(v => ({date: DateFormatter.formatDateString(v.date), price: v.value}));
    }
}
