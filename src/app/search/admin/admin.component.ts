import { Component, OnDestroy, OnInit } from '@angular/core';
import { SearchResultsClient } from '@core/search-results/search-results.client';
import { Subscription, Observable, of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-search-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit, OnDestroy {

    private sub: Subscription;
    public data: object;

    constructor(private readonly searchResultsClient: SearchResultsClient) {
    }

    ngOnInit() {
        this.sub = this.searchResultsClient.getSearchResultsSummary(this.onErrorResponse)
            .subscribe((v: object) => {
                this.data = v;
            });
    }

    private onErrorResponse = (error: HttpErrorResponse): Observable<object> => of([]);

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }
}
