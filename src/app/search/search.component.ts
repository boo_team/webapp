import { Component, OnDestroy, OnInit } from '@angular/core';
import { SocketService } from '@core/socket.service';
import { AccessService } from '@core/access.service';
import { SearchRequestsStore } from '@core/search-requests/search-requests.store';
import Socket = SocketIOClient.Socket;

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {

    private socket: Socket;

    constructor(private readonly accessService: AccessService,
                private readonly searchRequestsStore: SearchRequestsStore,
                private readonly socketService: SocketService) {
    }

    ngOnInit(): void {
        const accessToken = this.accessService.getToken();
        this.socket = this.socketService.connect(accessToken);
    }

    ngOnDestroy(): void {
        this.socketService.disconnect(this.socket);
    }
}
