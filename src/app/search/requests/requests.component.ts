import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, TrackByFunction } from '@angular/core';
import { Subscription } from 'rxjs';
import { SearchRequestDto, SearchRequestRaw } from '@bo/common';
import { DialogService } from '@core/dialog.service';
import { SnackbarService } from '@core/snackbar.service';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { switchMap } from 'rxjs/operators';
import { SearchRequestsStore } from '@core/search-requests/search-requests.store';

@Component({
    selector: 'app-search-requests',
    templateUrl: './requests.component.html',
    styleUrls: ['./requests.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequestsComponent implements OnInit, OnDestroy {

    public sub: Subscription;
    public searchRequests: SearchRequest[] = [];
    public readonly trackBySearchId: TrackByFunction<SearchRequest> = (index: number, item) => item.searchId;

    constructor(private readonly cd: ChangeDetectorRef,
                private readonly dialogService: DialogService,
                private readonly searchRequestsStore: SearchRequestsStore,
                private readonly snackbarService: SnackbarService) {
    }

    ngOnInit(): void {
        this.sub = this.searchRequestsStore.getSearchRequests().subscribe((v) => {
            this.searchRequests = v.sort((a, b) => a.updatedAt < b.updatedAt ? 1 : -1);
            this.cd.markForCheck();
        });
    }

    public createRequest = () => this.dialogService.openCreateSearchReqDialog()
        .pipe(
            switchMap((searchRequestRaw: SearchRequestRaw) =>
                this.searchRequestsStore.createSearchRequest(searchRequestRaw, this.onCreateSuccess)),
        ).subscribe()

    private onCreateSuccess = (searchRequestDto: SearchRequestDto) => {
        console.log('Search request successfully created: ', searchRequestDto);
        this.snackbarService.open('Search request has been created.');
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

}
