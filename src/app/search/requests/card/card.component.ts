import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { SearchRequestDto, SearchRequestRaw, SearchStatusDto } from '@bo/common';
import { DialogService } from '@core/dialog.service';
import { SnackbarService } from '@core/snackbar.service';
import { Subscription } from 'rxjs';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { SearchStatusService } from '@core/search-status/search-status.service';
import { SearchRequestHelper } from '@core/search-requests/utils/SearchRequestHelper';
import { SearchRequestStatus } from '@core/search-requests/model/SearchRequestStatus';
import { switchMap } from 'rxjs/operators';
import { SearchRequestsStore } from '@core/search-requests/search-requests.store';
import { DateFormatter } from '@core/utils/DateFormatter';

@Component({
    selector: 'app-search-requests-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent implements OnInit, OnDestroy {

    @Input() searchRequest: SearchRequest;
    public readonly searchRequestStatuses: typeof SearchRequestStatus = SearchRequestStatus;
    public readonly NOT_AVAILABLE = DateFormatter.NOT_AVAILABLE;
    private subscription: Subscription;

    // TODO: try to reduce number of switchMap before calls
    constructor(private readonly dialogService: DialogService,
                private readonly searchRequestsStore: SearchRequestsStore,
                private readonly searchStatusService: SearchStatusService,
                private readonly snackbarService: SnackbarService) {
    }

    ngOnInit(): void {
        this.subscription = this.searchStatusService.getSearchStatus(this.searchRequest.searchId)
            .subscribe((s: SearchStatusDto) => {
                this.searchRequest.status = SearchRequestHelper.isInProgress(s);
            });
    }

    public editRequest(oldSearchRequest: SearchRequest): void {
        this.dialogService.openCreateSearchReqDialog(oldSearchRequest, true)
            .pipe(
                switchMap((newSearchRequestRaw: SearchRequestRaw) =>
                    this.searchRequestsStore.updateSearchRequest(oldSearchRequest.searchId,
                        newSearchRequestRaw, this.onEditSuccess(oldSearchRequest))),
            ).subscribe();
    }

    public cloneRequest(oldSearchRequest: SearchRequest): void {
        this.dialogService.openCreateSearchReqDialog(oldSearchRequest)
            .pipe(
                switchMap((newSearchRequestRaw: SearchRequestRaw) =>
                    this.searchRequestsStore.createSearchRequest(newSearchRequestRaw, this.onCloneSuccess)),
            ).subscribe();
    }

    public deleteRequest(searchRequest: SearchRequest): void {
        this.dialogService.openConfirmationDialog({
            title: 'Are you sure to delete the search request?',
            content: 'Search results will be removed as well.',
        })
            .pipe(
                switchMap((yes: boolean) =>
                    this.searchRequestsStore.deleteSearchRequest(searchRequest.searchId, this.onDeleteSuccess(searchRequest))),
            ).subscribe();
    }

    private onEditSuccess = (oldSearchRequest: SearchRequest) =>
        (res: SearchRequestDto): void => {
            console.log(`Search request for search id: ${oldSearchRequest.searchId} successfully edited. New search request: `, res);
            this.snackbarService.open('Search request has been edited.');
        }

    private onCloneSuccess = (searchRequestDto: SearchRequestDto) => {
        console.log('Search request successfully cloned: ', searchRequestDto);
        this.snackbarService.open('Search request has been cloned.');
    }

    private onDeleteSuccess = (searchRequest: SearchRequest) =>
        (res: string): void => {
            console.log(`Search request for search id: ${searchRequest.searchId} succesfuly deleted. Res: `, res);
            this.snackbarService.open(`Search request has been deleted.`);
        }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
