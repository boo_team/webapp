import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, TrackByFunction, ViewChild } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { combineLatest, Observable, ReplaySubject, Subscription } from 'rxjs';
import { HotelSummary } from '@core/search-results/model/HotelSummary';
import { SearchResultsStore } from '@core/search-results/search-results.store';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { SearchResultSummary } from '@core/search-results/model/SearchResultSummary';
import { HotelsFilterFunc } from '@core/search-results/interface/hotelsFilter';
import { ResultsNavBarComponent } from '@results/nav-bar/results-nav-bar.component';

@Component({
    selector: 'app-search-results-grid',
    templateUrl: './search-results-grid.component.html',
    styleUrls: ['./search-results-grid.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsGridComponent implements OnInit, OnDestroy {

    @ViewChild(ResultsNavBarComponent) resultsNavBar: ResultsNavBarComponent;
    public hotelsSummary$: Observable<HotelSummary[]>;
    public readonly filters$: ReplaySubject<HotelsFilterFunc[]>;
    public cols = 4;
    private readonly breakpoints = {
        xl: 4,
        lg: 3,
        md: 2,
        sm: 1,
        xs: 1,
    };

    private mediaSub: Subscription;

    constructor(private readonly route: ActivatedRoute,
                private readonly cd: ChangeDetectorRef,
                private readonly mediaObserver: MediaObserver,
                private readonly searchResultsStore: SearchResultsStore) {
        this.mediaSub = new Subscription();
        this.filters$ = new ReplaySubject(1);
        this.filters$.next([]); // TODO: move to navbar
    }

    ngOnInit(): void {
        this.mediaSub = this.mediaObserver.asObservable().subscribe((mediaChange: MediaChange[]) => {
            this.cols = this.breakpoints[mediaChange[0].mqAlias];
            this.cd.markForCheck();
        });
        this.hotelsSummary$ = this.route.params.pipe(
            switchMap(({searchId}) => combineLatest([
                this.searchResultsStore.getSearchResultSummary(searchId),
                this.filters$,
            ])),
            map(([summary, filters]: [SearchResultSummary, HotelsFilterFunc[]]) =>
                filters.reduce((acc: HotelSummary[], fFunc) => acc.filter(fFunc), summary.hotelsSummary),
            ),
        );
    }

    public resetFilters() {
        this.resultsNavBar.resetFilters();
    }

    public readonly trackByHotelId: TrackByFunction<HotelSummary> = (index: number, item) => item.hotelId;

    ngOnDestroy(): void {
        this.mediaSub.unsubscribe();
    }
}
