import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { HotelSummary } from '@core/search-results/model/HotelSummary';

@Component({
    selector: 'app-search-results-tile-content',
    templateUrl: './tile-content.component.html',
    styleUrls: ['./tile-content.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileContentComponent {

    @Input() extended: boolean;
    public hotel: HotelSummary;

    @Input('hotel')
    set setHotel(hotel: HotelSummary) {
        this.hotel = hotel;
    }
}
