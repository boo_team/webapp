import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatGridTile } from '@angular/material';
import { HotelSummary } from '@core/search-results/model/HotelSummary';
import { DialogService } from '@core/dialog.service';
import { UserStore } from '@core/users/user.store';

@Component({
    selector: 'app-search-results-tile',
    templateUrl: './tile.component.html',
    styleUrls: ['./tile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TileComponent {

    @Input() gridElement: MatGridTile;
    public hotel: HotelSummary;
    public extended = false;

    @Input('hotel')
    set assignHotel(hotel: HotelSummary) {
        this.hotel = hotel;
    }

    constructor(private readonly dialogService: DialogService,
                private readonly userStore: UserStore) {
    }

    extendTile(numberOfDetails: string | null) {
        if (!numberOfDetails) {
            return;
        }
        if (this.extended) {
            this.gridElement.rowspan = 1;
        } else {
            this.gridElement.rowspan = 2;
        }
        this.extended = !this.extended;
    }

    toggleFavorite() {
        this.hotel.setIsFavorite(!this.hotel.isFavorite);
        this.userStore.emitFavoritesChange(this.hotel);
    }

    public openHistory(hotel: HotelSummary) {
        this.dialogService.openShowResultsHistoryDialog(hotel).subscribe();
    }
}
