import { Component, Input, OnInit } from '@angular/core';
import { SearchRequestsStore } from '@core/search-requests/search-requests.store';
import { Observable, ReplaySubject } from 'rxjs';
import { SearchRequest } from '@core/search-requests/model/SearchRequest';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { HotelsFilter, HotelsFilterFunc } from '@core/search-results/interface/hotelsFilter';
import { HotelSummary } from '@core/search-results/model/HotelSummary';

@Component({
    selector: 'app-search-results-nav-bar',
    templateUrl: './results-nav-bar.component.html',
    styleUrls: ['./results-nav-bar.component.scss'],
})
export class ResultsNavBarComponent implements OnInit {

    @Input() public filters$: ReplaySubject<HotelsFilterFunc[]>;
    @Input() public hotelsCount: number;
    public searchRequest$: Observable<SearchRequest>;
    public filters: { [key: string]: HotelsFilter } = {
        favorite: {
            active: false,
            predicate: (h: HotelSummary) => h.isFavorite,
            iconActive: 'favorite',
            iconInactive: 'favorite_border',
        },
        priceRate: {
            active: false,
            predicate: (h: HotelSummary) => h.priceRate > 0,
            iconActive: 'attach_money',
            iconInactive: 'money_off',
        },
    };

    constructor(private readonly route: ActivatedRoute,
                private readonly searchRequestsStore: SearchRequestsStore) {
    }

    ngOnInit() {
        this.searchRequest$ = this.route.params.pipe(
            switchMap(({searchId}) => this.searchRequestsStore.getSearchRequest(searchId)),
        );
    }

    public toggleFilter(filter: HotelsFilter): void {
        filter.active = !filter.active;
        this.emitActiveFilters(this.filters$, this.filters);
    }

    public resetFilters() {
        Object.keys(this.filters)
            .forEach((k) => {
                this.filters[k].active = false;
            });
        this.emitActiveFilters(this.filters$, this.filters);
    }

    private emitActiveFilters = (filters$: ReplaySubject<HotelsFilterFunc[]>,
                                 filters: { [key: string]: HotelsFilter }) => {
        const activeFiltersFunc = Object.keys(filters)
            .map((k) => filters[k])
            .filter((v) => v.active)
            .map((v) => v.predicate);
        filters$.next(activeFiltersFunc);
    }
}
