import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsNavBarComponent } from './results-nav-bar.component';

describe('ResultsNavBarComponent', () => {
  let component: ResultsNavBarComponent;
  let fixture: ComponentFixture<ResultsNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
