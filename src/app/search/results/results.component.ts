import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { SnackbarService } from '@core/snackbar.service';
import { SearchResultsStore } from '@core/search-results/search-results.store';

@Component({
    selector: 'app-search-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.scss'],
})
export class ResultsComponent implements OnInit {

    public resultExist = false;
    public loading = true;

    constructor(private readonly route: ActivatedRoute,
                private readonly searchResultsStore: SearchResultsStore,
                private readonly snackbarService: SnackbarService) {
    }

    ngOnInit() {
        this.route.params.pipe(
            switchMap(({searchId}) => {
                this.searchResultsStore.fetchSearchResultSummary(searchId, this.onErrorResponse);
                return this.searchResultsStore.getSearchResultSummary(searchId);
            }),
            first(),
        ).subscribe((v) => {
            this.resultExist = !!v;
            this.loading = false;
        });
    }

    private onErrorResponse = (error: HttpErrorResponse): Observable<null> => {
        this.snackbarService.openError(`Error [${error.status}] when aggregating search results.`);
        return of(null);
    }
}
