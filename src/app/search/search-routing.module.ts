import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchComponent } from '@app/search/search.component';
import { RequestsComponent } from '@requests/requests.component';
import { ResultsComponent } from '@results/results.component';
import { AdminComponent } from '@app/search/admin/admin.component';

const routes: Routes = [
    {
        path: '', component: SearchComponent,
        children: [
            {path: '', redirectTo: 'requests', pathMatch: 'full'},
            {path: 'requests', component: RequestsComponent},
            {path: 'results/:searchId', component: ResultsComponent}, // TODO: add resolver
            {path: 'admin', component: AdminComponent},
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SearchRoutingModule {
}
