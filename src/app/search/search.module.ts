import { NgModule } from '@angular/core';
import { NavBarComponent } from '@app/search/nav-bar/nav-bar.component';
import { AdminComponent } from '@app/search/admin/admin.component';
import { SearchComponent } from '@app/search/search.component';
import { SearchRoutingModule } from '@app/search/search-routing.module';
import { CommonModule } from '@angular/common';
import { RequestsComponent } from '@requests/requests.component';
import { CardComponent } from '@requests/card/card.component';
import { SearchResultsGridComponent } from '@results/grid/search-results-grid.component';
import { ResultsComponent } from '@results/results.component';
import { TileComponent } from '@results/tile/tile.component';
import { ResultsNavBarComponent } from '@results/nav-bar/results-nav-bar.component';
import { TileContentComponent } from '@results/tile-content/tile-content.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        SearchRoutingModule,
    ],
    declarations: [
        // search-requests
        RequestsComponent,
        CardComponent,
        // search-results
        ResultsComponent,
        SearchResultsGridComponent,
        TileComponent,
        ResultsNavBarComponent,
        TileContentComponent,
        // others
        NavBarComponent,
        AdminComponent,
        SearchComponent,
    ],
})
export class SearchModule {
}
