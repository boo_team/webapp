import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SnackbarService } from '@core/snackbar.service';
import { AccessService } from '@core/access.service';

@Component({
    selector: 'app-search-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit, OnDestroy {

    constructor(private readonly accessService: AccessService,
                private readonly snackbarService: SnackbarService) {
    }

    ngOnInit() {
    }

    public isAdmin = (): Observable<boolean> => this.accessService.isAdmin();

    public logout() {
        this.accessService.logout();
        this.snackbarService.open('Successfully log out.');
    }

    ngOnDestroy(): void {
    }
}
